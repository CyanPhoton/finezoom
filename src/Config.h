#pragma once
#include "f4se_common/f4se_version.h"

#define MAKE_VERSION(major, minor, patch) ((major & 0xFF) << 16) | ((minor & 0xFF) << 8) | (patch & 0xFF)

//-----------------------
// Plugin Information
//-----------------------
#define PLUGIN_VERSION               MAKE_VERSION(0,3,7)
#define PLUGIN_VERSION_STRING       "0.3.7"
#define PLUGIN_NAME_SHORT           "FineZoom"
#define PLUGIN_NAME_LONG            "FineZoom"
#define SUPPORTED_RUNTIME_VERSION   CURRENT_RELEASE_RUNTIME
#define MAXIMUM_RUNTIME_VERSION     RUNTIME_VERSION_1_10_163
#define MINIMUM_RUNTIME_VERSION     RUNTIME_VERSION_1_10_130