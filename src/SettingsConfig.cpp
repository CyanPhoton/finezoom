#include <fstream>
#include <winbase.h>
#include <string>
#include <sstream>
#include "SettingsConfig.h"
#include "../common/IDebugLog.h"
#include "Logic.h"

SettingsConfig::Settings SettingsConfig::settings = SettingsConfig::Settings{};
struct stat SettingsConfig::fileStat{};
bool SettingsConfig::validFileStat = false;

void SettingsConfig::loadSettings() {
    settings = Settings{};

    const char *fileName = "Data/MCM/Settings/FineZoom.ini";

    auto fileStream = std::ifstream(fileName, std::ifstream::in);
    if (!fileStream.is_open()) {
        validFileStat = false;
        return;
    }

    validFileStat = stat(fileName, &fileStat) == 0;

    // General
    settings.zoomRate = getFloat("General", "fZoomRate", 1.2f, fileName);
    settings.minZoomFactor = getFloat("General", "fMinZoomFactor", 0.5f, fileName);
    settings.hasMaxZoomFactor = GetPrivateProfileIntA("General", "bHasMaxFactor", 1, fileName) == 1;
    settings.maxZoomFactor = getFloat("General", "fMaxZoomFactor", 10.0f, fileName);
    settings.resetZoomOnLowerScopeMode = GetPrivateProfileIntA("General", "iResetZoomOnLowerScope", 0, fileName);
    settings.resetZoomWhileNotScope = GetPrivateProfileIntA("General", "bResetZoomWhileNotScope", 0, fileName) == 1;
    settings.zoomCurveType = GetPrivateProfileIntA("General", "iZoomCurveType", 0, fileName);
    settings.smoothStep = GetPrivateProfileIntA("General", "bSmoothStep", 1, fileName) == 1;
    settings.smoothReset = GetPrivateProfileIntA("General", "bSmoothReset", 1, fileName) == 1;
    settings.staticModelGlowIron = GetPrivateProfileIntA("General", "bStaticModelIronGlow", 1, fileName) == 1;
    settings.staticModelReflex = GetPrivateProfileIntA("General", "bStaticModelReflex", 1, fileName) == 1;
    settings.staticModeSTS = GetPrivateProfileIntA("General", "bStaticModelSTS", 1, fileName) == 1;
    settings.rememberAfterSwitch = GetPrivateProfileIntA("General", "bRememberAfterSwitch", 1, fileName) == 1;

    // Controls - Mouse
    settings.reverseScrollDirection = GetPrivateProfileIntA("Controls - Mouse", "bReversedScroll", 0, fileName) == 1;

    // Controls - Controller
    settings.dPadZoomFactor = getFloat("Controls - Controller", "fDPadZoomFactor", 0.035f, fileName);
    settings.enableFastDPad = GetPrivateProfileIntA("Controls - Controller", "bEnableFastDPad", 1, fileName) == 1;
    settings.fastDPadFactor = getFloat("Controls - Controller", "fFastDPadFactor", 2.0f, fileName);
    settings.reverseDPadZoom = GetPrivateProfileIntA("Controls - Controller", "bReverseDPadZoom", 0, fileName) == 1;
    settings.reverseDPadFastZoom = GetPrivateProfileIntA("Controls - Controller", "bReverseDPadFastZoom", 0, fileName) == 1;
    settings.controllerResetZoomButton = GetPrivateProfileIntA("Controls - Controller", "iControllerZoomResetButton", 0, fileName);

    // Immersion
    settings.immersionMode = GetPrivateProfileIntA("Immersion", "iImmersionMode", 0, fileName);
    settings.useIronSights = GetPrivateProfileIntA("Immersion", "bIronSights", 0, fileName) == 1;
    settings.useImprovedIronSights = GetPrivateProfileIntA("Immersion", "bImprovedIronSights", 0, fileName) == 1;
    settings.useGlowSights = GetPrivateProfileIntA("Immersion", "bGlowSights", 0, fileName) == 1;
    settings.useReflexSights = GetPrivateProfileIntA("Immersion", "bReflexSights", 0, fileName) == 1;
    settings.useProperNormalSights = GetPrivateProfileIntA("Immersion", "bProperNormal", 0, fileName) == 1;
    settings.useProperNVSights = GetPrivateProfileIntA("Immersion", "bProperNV", 0, fileName) == 1;
    settings.useProperReconSights = GetPrivateProfileIntA("Immersion", "bProperRecon", 0, fileName) == 1;
    settings.useOtherSights = GetPrivateProfileIntA("Immersion", "bOther", 0, fileName) == 1;
    settings.useSTS_Normal = GetPrivateProfileIntA("Immersion", "bSTS_Normal", 0, fileName) == 1;
    settings.useSTS_NV = GetPrivateProfileIntA("Immersion", "bSTS_NV", 0, fileName) == 1;
    settings.useSTS_Recon = GetPrivateProfileIntA("Immersion", "bSTS_Recon", 0, fileName) == 1;

    std::stringstream debugOutput{};
    debugOutput << settings;
    _MESSAGE("Loaded info: %s", debugOutput.str().c_str());
    Logic::switchWeapons();
}

void SettingsConfig::updateSettings() {
    if (!validFileStat) {
        loadSettings();
        return;
    }

    const char *fileName = "Data/MCM/Settings/FineZoom.ini";
    struct stat newFileStat{};

    if (stat(fileName, &newFileStat) == 0) {
        if (newFileStat.st_mtime > fileStat.st_mtime) {
            loadSettings();
        }
    } else {
        loadSettings();
    }

}

float SettingsConfig::getFloat(LPCSTR section, LPCSTR name, float defaultValue, LPCSTR fileName) {

    char buffer[1024];
    GetPrivateProfileStringA(section, name, std::to_string(defaultValue).c_str(), buffer, 1024, fileName);

    try {
        float value = std::stof(std::string(buffer));
        return value;
    } catch (std::exception &e) {
        (void) e;
        return defaultValue;
    }
}

std::ostream &operator<<(std::ostream &os, const SettingsConfig::Settings &settings) {
    os << "zoomRate: " << settings.zoomRate << " minZoomFactor: " << settings.minZoomFactor << " hasMaxZoomFactor: " << settings.hasMaxZoomFactor << " maxZoomFactor: " << settings.maxZoomFactor << " resetZoomOnLowerScopeMode: "
       << settings.resetZoomOnLowerScopeMode << " zoomCurveType: " << settings.zoomCurveType << " smoothStep: " << settings.smoothStep << " smoothReset: " << settings.smoothReset << " staticModelGlowIron: " << settings.staticModelGlowIron
       << " staticModelReflex: " << settings.staticModelReflex << " staticModeSTS: " << settings.staticModeSTS << " rememberAfterSwitch: " << settings.rememberAfterSwitch << " reverseScrollDirection: " << settings.reverseScrollDirection
       << " resetZoomWhileNotScope: " << settings.resetZoomWhileNotScope << " dPadZoomFactor: " << settings.dPadZoomFactor << " enableFastDPad: " << settings.enableFastDPad << " fastDPadFactor: " << settings.fastDPadFactor
       << " reverseDPadZoom: " << settings.reverseDPadZoom << " reverseDPadFastZoom: " << settings.reverseDPadFastZoom << " controllerResetZoomButton: " << settings.controllerResetZoomButton << " immersionMode: " << settings.immersionMode
       << " useIronSights: " << settings.useIronSights << " useImprovedIronSights: " << settings.useImprovedIronSights << " useGlowSights: " << settings.useGlowSights << " useReflexSights: " << settings.useReflexSights
       << " useProperNormalSights: " << settings.useProperNormalSights << " useProperNVSights: " << settings.useProperNVSights << " useProperReconSights: " << settings.useProperReconSights << " useOtherSights: " << settings.useOtherSights
       << " useSTS_Normal: " << settings.useSTS_Normal << " useSTS_NV: " << settings.useSTS_NV << " useSTS_Recon: " << settings.useSTS_Recon;
    return os;
}
