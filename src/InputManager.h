#pragma once

#include <windows.h>
#include <unordered_set>

class InputManager {
    static HHOOK mouseHook;
public:
    static void setupInput(HINSTANCE hinstance, DWORD threadID);
    static void update();

    static const HHOOK getMouseHook();
    static void cleanUp();
};
