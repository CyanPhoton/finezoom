#include <winuser.h>
#include <processthreadsapi.h>
#include <sstream>
#include <cmath>
#include "../common/IDebugLog.h"
#include "f4se_common/f4se_version.h"

#include "Config.h"
#include "Logic.h"
#include "MemoryManager.h"
#include "PapyrusFineZoom.h"
#include "InputManager.h"
#include "SettingsConfig.h"

using namespace std::chrono_literals;

PluginHandle Logic::pluginHandle = kPluginHandle_Invalid;
F4SEPapyrusInterface *Logic::papyrusInterface = nullptr;
F4SEMessagingInterface *Logic::messagingInterface = nullptr;

DWORD Logic::threadID = 0;
HINSTANCE Logic::hInstance = nullptr;

bool Logic::terminate = false;
std::thread Logic::mainThread{};

std::atomic<float> Logic::zoomFactor(1.0f);
std::atomic<float> Logic::zoomDelta(0.0f);
std::atomic<float> Logic::defaultZoomDelta(-1.0f);
std::atomic<float> Logic::defaultModelFOVBase(-1.0f);

size_t Logic::lastWeaponID = 0;
std::unordered_map<std::uintptr_t, float> Logic::lastZoomFactors{};
std::mutex Logic::lastZoomFactorsMutex{};

std::atomic_bool Logic::isSighted(false);
std::atomic_bool Logic::validDefaultZoomDelta(false);

std::atomic_bool Logic::needToResetZoomLevel(false);

const std::atomic_int32_t Logic::smoothSteps(50);
std::atomic_int32_t Logic::smoothProgress(50);
std::atomic_bool Logic::smoothing(false);
std::atomic<float> Logic::previousZoomFactor(1.0f);
std::atomic<float> Logic::previousTargetZoomFactor(1.0f);

std::atomic<Logic::ScopeType> Logic::scopeType{Logic::ScopeType::Other};

std::atomic_bool Logic::isMenuMode(false);
std::atomic_bool Logic::isScopeMenu(false);

void Logic::setHInstance(HINSTANCE hinstance) {
    hInstance = hinstance;
}

void Logic::setInfo(PluginInfo *info) {
    // populate info structure
    info->infoVersion = PluginInfo::kInfoVersion;
    info->name = PLUGIN_NAME_SHORT;
    info->version = PLUGIN_VERSION;
}

bool Logic::query(const F4SEInterface *f4se) {

    pluginHandle = f4se->GetPluginHandle();

    // Get the papyrus interface
    papyrusInterface = (F4SEPapyrusInterface *) f4se->QueryInterface(kInterface_Papyrus);
    if (!papyrusInterface) {
        _MESSAGE("Couldn't get papyrus interface");
        return false;
    }

    // Get the messaging interface
    messagingInterface = (F4SEMessagingInterface *) f4se->QueryInterface(kInterface_Messaging);
    if (!messagingInterface) {
        _MESSAGE("Couldn't get messaging interface");
        return false;
    }

    return true;
}

bool RegisterPapyrus(VirtualMachine *vm) {
    PapyrusFineZoom::RegisterFuncs(vm);
    _MESSAGE("Registered Papyrus native functions.");

    return true;
}

void OnF4SEMessage(F4SEMessagingInterface::Message *msg);

bool Logic::load(const F4SEInterface *f4se) {

    std::stringstream runtimeMemFileStringS{};
    runtimeMemFileStringS << "Data/F4SE/Plugins/FineZoomMem/FineZoomMem_";
    runtimeMemFileStringS << "v" << GET_EXE_VERSION_MAJOR(f4se->runtimeVersion);
    runtimeMemFileStringS << "." << GET_EXE_VERSION_MINOR(f4se->runtimeVersion);
    runtimeMemFileStringS << "." << GET_EXE_VERSION_BUILD(f4se->runtimeVersion);
    runtimeMemFileStringS << "." << GET_EXE_VERSION_SUB(f4se->runtimeVersion);
    runtimeMemFileStringS << ".ini";

    std::string runtimeMemFileString = runtimeMemFileStringS.str();

    if (!MemoryManager::loadMemoryAddresses(runtimeMemFileString)) {
        {
            std::stringstream errorMessageStringS{};
            errorMessageStringS << "FineZoom failed to load memory address file, for this Fallout 4 version the expected filepath is: [";
            errorMessageStringS << runtimeMemFileString;
            errorMessageStringS << "], please make sure it exists and is valid, attempting to default to latest";

            _MESSAGE("%s", errorMessageStringS.str().c_str());
        }

        std::string defaultPath = "Data/F4SE/Plugins/FineZoomMem/FineZoomMem.ini";

        if (!MemoryManager::loadMemoryAddresses(defaultPath)) {
            std::stringstream errorMessageStringS{};
            errorMessageStringS << "FineZoom failed to load default memory address file,\n after failing to find version specific file for this Fallout 4 version,\n the expected filepath for which being: \n[";
            errorMessageStringS << runtimeMemFileString;
            errorMessageStringS << "],\n please make sure your install is valid, \nFineZoom, will be disabled";

            _MESSAGE("%s", errorMessageStringS.str().c_str());
            MessageBox(nullptr, errorMessageStringS.str().c_str(), "Fine Zoom", MB_OK | MB_ICONERROR);
            return false;
        }
    }

    threadID = GetCurrentThreadId();

    // Initialize globals and addresses
    papyrusInterface->Register(RegisterPapyrus);
    messagingInterface->RegisterListener(pluginHandle, "F4SE", OnF4SEMessage);

    return true;
}

void OnF4SEMessage(F4SEMessagingInterface::Message *msg) {
    switch (msg->type) {
        case F4SEMessagingInterface::kMessage_GameLoaded:
            Logic::main();
            break;
    }
}

bool Logic::isSightedStateValid() {
    if (SettingsConfig::settings.immersionMode == 0) {
        return isSighted && !isMenuMode && (MemoryManager::getIsFirstPerson() || isScopeMenu);
    } else if (SettingsConfig::settings.immersionMode == 1) {
        return isSighted && !isMenuMode && (MemoryManager::getIsFirstPerson() || isScopeMenu) && (scopeType == ScopeType::NormalSight || scopeType == ScopeType::NormalNightVision || scopeType == ScopeType::NormalRecon);
    } else if (SettingsConfig::settings.immersionMode == 2) {
        return isSighted && !isMenuMode && (MemoryManager::getIsFirstPerson() || isScopeMenu) && (scopeType == ScopeType::NormalSight || scopeType == ScopeType::NormalNightVision || scopeType == ScopeType::NormalRecon || scopeType == ScopeType::STS_Normal || scopeType == ScopeType::STS_NV || scopeType == ScopeType::STS_Recon);
    } else if (SettingsConfig::settings.immersionMode == 3) {
        return isSighted && !isMenuMode && (MemoryManager::getIsFirstPerson() || isScopeMenu) && (
                    (SettingsConfig::settings.useIronSights             && scopeType == ScopeType::IronSights           ) ||
                    (SettingsConfig::settings.useImprovedIronSights     && scopeType == ScopeType::ImprovedIronSights   ) ||
                    (SettingsConfig::settings.useGlowSights             && scopeType == ScopeType::GlowSights           ) ||
                    (SettingsConfig::settings.useReflexSights           && scopeType == ScopeType::ReflexSight          ) ||
                    (SettingsConfig::settings.useProperNormalSights     && scopeType == ScopeType::NormalSight          ) ||
                    (SettingsConfig::settings.useProperNVSights         && scopeType == ScopeType::NormalNightVision    ) ||
                    (SettingsConfig::settings.useProperReconSights      && scopeType == ScopeType::NormalRecon          ) ||
                    (SettingsConfig::settings.useOtherSights            && scopeType == ScopeType::Other                ) ||
                    (SettingsConfig::settings.useSTS_Normal             && scopeType == ScopeType::STS_Normal           ) ||
                    (SettingsConfig::settings.useSTS_NV                 && scopeType == ScopeType::STS_NV               ) ||
                    (SettingsConfig::settings.useSTS_Recon              && scopeType == ScopeType::STS_Recon            )
                );
    }
    return false;
}

void Logic::main() {

    mainThread = std::thread([&] {

        InputManager::setupInput(hInstance, threadID);
        SettingsConfig::loadSettings();

        const int steps = 100;
        int progress = 0;

        while (!terminate) {
            while (isMenuMode) {
                std::this_thread::sleep_for(1ms);
                InputManager::update();
                zoomDelta = 0;
            }

            std::this_thread::sleep_for(1ms);
            InputManager::update();

            bool validSightedState = isSightedStateValid();

            if (validSightedState && validDefaultZoomDelta) {
                zoomFactor = maximum(zoomFactor * powf(SettingsConfig::settings.zoomRate, zoomDelta), maximum(SettingsConfig::settings.minZoomFactor, 1.0f + (defaultZoomDelta / MemoryManager::getActiveBaseFOV())));
                if (SettingsConfig::settings.hasMaxZoomFactor) {
                    zoomFactor = minimum(zoomFactor, SettingsConfig::settings.maxZoomFactor);
                }
            }
            zoomDelta = 0;

            bool wasReset = false;
            if (needToResetZoomLevel) {
                if (SettingsConfig::settings.resetZoomWhileNotScope || validSightedState) {
                    zoomFactor = 1.0f;
                    wasReset = true;
                }
                needToResetZoomLevel = false;
            }

            if (validDefaultZoomDelta) {

                if (validSightedState && progress < steps + 20) { // 20 from at 60fps frame time is 16ms, plus a extra 4ms buffer, maybe adjustable later?
                    progress++;
                } else if (!validSightedState && progress > 0) {
                    progress--;
                    if (progress == 0) {
                        if (SettingsConfig::settings.resetZoomOnLowerScopeMode == 1) {
                            zoomFactor = 1.0f;
                        }
                    } else if (progress < steps) {
                        if (SettingsConfig::settings.resetZoomOnLowerScopeMode == 2) {
                            zoomFactor = 1.0f;
                        }
                    }
                }
                if (validSightedState && smoothProgress < smoothSteps) {
                    smoothProgress++;
                }
                bool smoothStep = SettingsConfig::settings.smoothStep;
                bool smoothReset = SettingsConfig::settings.smoothReset;
                if (zoomFactor != previousZoomFactor && smoothStep) {
                    if (progress >= steps && smoothProgress >= smoothSteps && !smoothing) {
                        smoothProgress = 0;
                        smoothing = true;
                    } else if (smoothProgress >= smoothSteps){
                        previousZoomFactor = zoomFactor.load();
                        smoothing = false;
                    }
                }
                if (previousTargetZoomFactor != zoomFactor && smoothStep) {
                    smoothing = true;
                    previousZoomFactor = interpolate(previousZoomFactor, previousTargetZoomFactor, (float) smoothProgress / smoothSteps, 0);
                    smoothProgress = 0;
                    previousTargetZoomFactor = zoomFactor.load();
                }
                if (wasReset && smoothStep && !smoothReset) {
                    previousZoomFactor = zoomFactor.load();
                    previousTargetZoomFactor = zoomFactor.load();
                    smoothProgress = smoothSteps.load();
                    smoothing = false;
                }

                float smoothedZoomFactor = interpolate(previousZoomFactor, zoomFactor, (float) smoothProgress / smoothSteps, 0);

                MemoryManager::getFOVAdjustSpeed() = 0.0f;
                MemoryManager::getCurrentFOVDelta() = interpolate(0.0f, 1.0f, (float) progress / steps, SettingsConfig::settings.zoomCurveType) * ((1.0f / smoothedZoomFactor) * (MemoryManager::getActiveBaseFOV() + defaultZoomDelta) - MemoryManager::getActiveBaseFOV());
                MemoryManager::getFOVAdjustSpeed() = 0.0f;

                if (
                        (scopeType == ScopeType::NormalSight || scopeType == ScopeType::NormalRecon || scopeType == ScopeType::NormalNightVision) ||
                        (scopeType == ScopeType::ReflexSight && SettingsConfig::settings.staticModelReflex) ||
                        ((scopeType == ScopeType::GlowSights || scopeType == ScopeType::IronSights || scopeType == ScopeType::ImprovedIronSights) && SettingsConfig::settings.staticModelGlowIron) ||
                        ((scopeType == ScopeType::STS_Normal || scopeType == ScopeType::STS_Recon || scopeType == ScopeType::STS_NV) && SettingsConfig::settings.staticModeSTS)
                    ) {
                    MemoryManager::getBaseModelFOV() = defaultModelFOVBase - (MemoryManager::getCurrentFOVDelta() - interpolate(0.0f, defaultZoomDelta, (float) progress / steps, SettingsConfig::settings.zoomCurveType));
                }

                MemoryManager::getFOVAdjustSpeed() = 0.0f;
            } else if (validSightedState) {
                int counter = 0;
                while (isSighted && MemoryManager::getIsFirstPerson() && counter < 50) {
                    std::this_thread::sleep_for(1ms);
                    counter++;
                }

                if (counter == 50) {
                    validDefaultZoomDelta = true;
                    defaultZoomDelta = MemoryManager::getTargetFOVDelta();
                    defaultModelFOVBase = MemoryManager::getBaseModelFOV();
                    progress = steps;
                    _MESSAGE("New Valid Delta Zoom: %f", defaultZoomDelta.load());
                    _MESSAGE("New Valid Base Model FOV: %f", defaultModelFOVBase.load());

                    counter = 0;
                    while (isSightedStateValid() && (MemoryManager::getCurrentFOVDelta() - defaultZoomDelta) > 0.01f ) {
                        counter++;
                        std::this_thread::sleep_for(1ms);
                    }
                    if (isSightedStateValid()) {
                        _MESSAGE("Extra count: %d", counter);
                    } else {
                        _MESSAGE("Unsighted early: %d", counter);
                    }
                }
            }

            SettingsConfig::updateSettings();
        }

        InputManager::cleanUp();
    });
}

void Logic::gameLoad() {
    lastZoomFactorsMutex.lock();
    lastWeaponID = 0;
    lastZoomFactors.clear();
    lastZoomFactorsMutex.unlock();

    if (validDefaultZoomDelta) {
        MemoryManager::getBaseModelFOV() = defaultModelFOVBase;
    }
    validDefaultZoomDelta = false;
    isSighted = false;
    zoomFactor = 1.0f;
    previousZoomFactor = zoomFactor.load();
    previousTargetZoomFactor = zoomFactor.load();
    smoothProgress = smoothSteps.load();
    smoothing = false;
}

void* getPtr() {
    if (!g_player.GetConst()) {
        return nullptr;
    }

    if (!(*g_player.GetConst())) {
        return nullptr;
    }

    if (!(*g_player.GetConst())->middleProcess) {
        return nullptr;
    }

    if (!(*g_player.GetConst())->middleProcess->unk08) {
        return nullptr;
    }

    if (!(*g_player.GetConst())->middleProcess->unk08->equipData) {
        return nullptr;
    }

    return (*g_player.GetConst())->middleProcess->unk08->equipData->instanceData;
}

void Logic::switchWeapons() {
    auto newWeapon = getPtr();

    bool rememberAfterSwitch = SettingsConfig::settings.rememberAfterSwitch;

    auto newWeaponID = reinterpret_cast<std::uintptr_t>(newWeapon);
    _MESSAGE("Switched weapon has 'ID': %d", newWeaponID);
    if (newWeapon != nullptr && rememberAfterSwitch) {

        lastZoomFactorsMutex.lock();
        if (lastWeaponID != 0) {
            lastZoomFactors[lastWeaponID] = zoomFactor;
        }
        lastWeaponID = newWeaponID;
    }

    if (validDefaultZoomDelta) {
        MemoryManager::getBaseModelFOV() = defaultModelFOVBase;
    }
    validDefaultZoomDelta = false;
    isSighted = false;

    if (newWeapon != nullptr) {
        if (lastZoomFactors.count(newWeaponID) > 0 && rememberAfterSwitch) {
            zoomFactor = lastZoomFactors[newWeaponID];
        } else {
            zoomFactor = 1.0f;
        }
    }

    previousZoomFactor = zoomFactor.load();
    previousTargetZoomFactor = zoomFactor.load();
    smoothProgress = smoothSteps.load();
    smoothing = false;

    if (newWeapon != nullptr && rememberAfterSwitch) {
        lastZoomFactorsMutex.unlock();
    }
}

void Logic::enterSighted() {
    isSighted = true;
    previousZoomFactor = zoomFactor.load();
    previousTargetZoomFactor = zoomFactor.load();
    smoothProgress = smoothSteps.load();
    smoothing = false;
}

void Logic::exitSighted() {
    isSighted = false;
    previousZoomFactor = zoomFactor.load();
    previousTargetZoomFactor = zoomFactor.load();
    smoothProgress = smoothSteps.load();
    smoothing = false;
}

void Logic::setScopeInfo(bool hasIronSights, bool hasImprovedIronSights, bool hasGlowSights, bool hasReflexSights, bool hasScope, bool hasNV, bool hasRecon, bool hasLaserMuzzle) {
    _MESSAGE("hasIronSights: %d hasImprovedIronSights: %d hasGlowSights: %d hasReflexSights: %d hasScope: %d hasNV: %d hasRecon: %d hasLaserMuzzle: %d", hasIronSights, hasImprovedIronSights, hasGlowSights, hasReflexSights, hasScope, hasNV, hasRecon, hasLaserMuzzle);
    if (hasIronSights && !hasImprovedIronSights && !hasGlowSights && !hasReflexSights && !hasScope && !hasNV && !hasRecon && !hasLaserMuzzle) {
        scopeType = ScopeType::IronSights;
        _MESSAGE("ScopeType::IronSights");
    } else if (hasIronSights && hasImprovedIronSights && !hasGlowSights && !hasReflexSights && !hasScope && !hasNV && !hasRecon && !hasLaserMuzzle) {
        scopeType = ScopeType::ImprovedIronSights;
        _MESSAGE("ScopeType::ImprovedIronSights");
    } else if (hasIronSights && !hasImprovedIronSights && hasGlowSights && !hasReflexSights && !hasScope && !hasNV && !hasRecon && !hasLaserMuzzle) {
        scopeType = ScopeType::GlowSights;
        _MESSAGE("ScopeType::GlowSights");
    } else if (!hasIronSights && !hasImprovedIronSights && !hasGlowSights && hasReflexSights && !hasScope && !hasNV && !hasRecon && !hasLaserMuzzle) {
        scopeType = ScopeType::ReflexSight;
        _MESSAGE("ScopeType::ReflexSight");
    } else if (!hasIronSights && !hasImprovedIronSights && !hasGlowSights && !hasReflexSights && hasScope && !hasNV && !hasRecon && !hasLaserMuzzle) {
        scopeType = ScopeType::NormalSight;
        _MESSAGE("ScopeType::NormalSight");
    } else if (!hasIronSights && !hasImprovedIronSights && !hasGlowSights && !hasReflexSights && hasScope && hasNV && !hasRecon && !hasLaserMuzzle) {
        scopeType = ScopeType::NormalNightVision;
        _MESSAGE("ScopeType::NormalNightVision");
    } else if (!hasIronSights && !hasImprovedIronSights && !hasGlowSights && !hasReflexSights && hasScope && !hasNV && hasRecon && !hasLaserMuzzle) {
        scopeType = ScopeType::NormalRecon;
        _MESSAGE("ScopeType::NormalRecon");
    } else if (!hasIronSights && !hasImprovedIronSights && !hasGlowSights && /*hasReflexSights &&*/ !hasScope && !hasNV && !hasRecon && hasLaserMuzzle) {
        scopeType = ScopeType::STS_Normal;
        _MESSAGE("ScopeType::STS_Normal");
    } else if (!hasIronSights && !hasImprovedIronSights && !hasGlowSights && /*hasReflexSights &&*/ !hasScope && hasNV && !hasRecon && hasLaserMuzzle) {
        scopeType = ScopeType::STS_NV;
        _MESSAGE("ScopeType::STS_NV");
    } else if (!hasIronSights && !hasImprovedIronSights && !hasGlowSights && /*hasReflexSights &&*/ !hasScope && !hasNV && hasRecon && hasLaserMuzzle) {
        scopeType = ScopeType::STS_Recon;
        _MESSAGE("ScopeType::STS_Recon");
    } else {
        scopeType = ScopeType::Other;
        _MESSAGE("ScopeType::Other");
    }
    switchWeapons();
}

void Logic::setIsMenuMode(bool value) {
    isMenuMode = value;
}

void Logic::setIsScopeMenu(bool value) {
    isScopeMenu = value;
}

void Logic::changeZoom(float delta) {
    if (!validDefaultZoomDelta)
        return;

    zoomDelta = zoomDelta + delta * SettingsConfig::Settings().zoomRate;
}

void Logic::resetZoomLevel() {
    needToResetZoomLevel = true;
}

void Logic::cleanUp() {
    terminate = true;
    if (mainThread.joinable()) {
        mainThread.join();
    }
}

#define _USE_MATH_DEFINES
#include <math.h>

float logisticCurve(float x) {
    const float L = 1.0f;
    const float A = -200.0f;
    const float B = -10.0f;

    float result = L / (1 - (A * pow(M_E, B * x))); // [0,1] -> [0.004975, 0.991002]
    return (result - 0.004975) / (0.991002 - 0.004975); // [0.004975, 0.991002] -> [0,1]
}

float sinCurve(float x) {
    return sinf((x - 0.5f) * M_PI) * 0.5 + 0.5;
}

float Logic::interpolate(float v1, float v2, float factor, int curveType) {
    factor = clamp(factor, 0.0f, 1.0f);
    if (curveType == 1) {
        factor = clamp(sinCurve(factor), 0.0f, 1.0f);
    } else if (curveType == 2) {
        factor = clamp(logisticCurve(factor), 0.0f, 1.0f);
    }
    return mix(v1, v2, factor);
}

float Logic::clamp(float v, float l1, float l2) {
    float min = minimum(l1, l2);
    float max = maximum(l1, l2);

    v = maximum(min, v);
    v = minimum(max, v);
    return v;
}

float Logic::mix(float v1, float v2, float factor) {
    return v1 * (1.0f - factor) + v2 * factor;
}

float Logic::maximum(float v1, float v2) {
    return v1 >= v2 ? v1 : v2;
}

float Logic::minimum(float v1, float v2) {
    return v1 <= v2 ? v1 : v2;
}
