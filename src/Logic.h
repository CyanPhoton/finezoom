#pragma once

#include <thread>
#include <atomic>
#include <unordered_map>
#include <mutex>
#include "f4se/f4se/GameReferences.h"
#include "f4se/f4se/PluginAPI.h"

class Logic {
public:
    enum ScopeType {
        IronSights,
        ImprovedIronSights,
        GlowSights,
        ReflexSight,
        NormalSight,
        NormalNightVision,
        NormalRecon,
        Other,
        STS_Normal,
        STS_NV,
        STS_Recon,
    };
private:
    static PluginHandle pluginHandle;
    static F4SEPapyrusInterface *papyrusInterface;
    static F4SEMessagingInterface *messagingInterface;

    static HINSTANCE hInstance;
    static DWORD threadID;

    static bool terminate;
    static std::thread mainThread;

    static std::atomic<float> zoomFactor;
    static std::atomic<float> zoomDelta;
    static std::atomic<float> defaultZoomDelta;
    static std::atomic<float> defaultModelFOVBase;

    static size_t lastWeaponID;
    static std::unordered_map<std::uintptr_t, float> lastZoomFactors;
    static std::mutex lastZoomFactorsMutex;

    static std::atomic_bool isSighted;
    static std::atomic_bool validDefaultZoomDelta;

    static std::atomic_bool needToResetZoomLevel;

    const static std::atomic_int32_t smoothSteps;
    static std::atomic_int32_t smoothProgress;
    static std::atomic_bool smoothing;
    static std::atomic<float> previousZoomFactor;
    static std::atomic<float> previousTargetZoomFactor;

    static std::atomic<ScopeType> scopeType;

    static std::atomic_bool isMenuMode;
    static std::atomic_bool isScopeMenu;
public:
    static void setHInstance(HINSTANCE hinstance);
    static void setInfo(PluginInfo *info);
    static bool query(const F4SEInterface *f4se);
    static bool load(const F4SEInterface *f4se);
    static void main();

    static void gameLoad();
    static void switchWeapons();
    static void enterSighted();
    static void exitSighted();
    static void setScopeInfo(bool hasIronSights, bool hasImprovedIronSights, bool hasGlowSights, bool hasReflexSights, bool hasScope, bool hasNV, bool hasRecon, bool hasLaserMuzzle);
    static void setIsMenuMode(bool value);
    static void setIsScopeMenu(bool value);

    static void changeZoom(float delta);
    static void resetZoomLevel();

    static void cleanUp();

private:
    static float interpolate(float v1, float v2, float factor, int curveType);
    static bool isSightedStateValid();

    static float clamp(float v, float l1, float l2);
    static float mix(float v1, float v2, float factor);
    static float maximum(float v1, float v2);
    static float minimum(float v1, float v2);
};
