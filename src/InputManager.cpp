#include "InputManager.h"

#include "../common/IDebugLog.h"
#include "MemoryManager.h"
#include "Logic.h"
#include "SettingsConfig.h"
#include <GLFW/glfw3.h>

HHOOK InputManager::mouseHook{};

static LRESULT CALLBACK MouseCallback(int code, WPARAM wparam, LPARAM lparam) {

    if (code < 0) {
        return CallNextHookEx(InputManager::getMouseHook(), code, wparam, lparam);
    }

    auto *info = reinterpret_cast<MOUSEHOOKSTRUCTEX *>(lparam);

    if (wparam == WM_MOUSEWHEEL) {
        short delta = HIWORD(info->mouseData);
        if (delta > 0) {
            Logic::changeZoom(SettingsConfig::settings.reverseScrollDirection ? -1.0f : 1.0f);
            _MESSAGE("Scroll in");
        } else if (delta < 0) {
            Logic::changeZoom(SettingsConfig::settings.reverseScrollDirection ? 1.0f : -1.0f);
            _MESSAGE("Scroll out");
        }
    }

    return CallNextHookEx(InputManager::getMouseHook(), code, wparam, lparam);
}

void InputManager::setupInput(HINSTANCE hInstance, DWORD threadID) {
    mouseHook = SetWindowsHookEx(WH_MOUSE, &MouseCallback, hInstance, threadID);
    bool glfwInitSuccess = glfwInit() == GLFW_TRUE;
    _MESSAGE("GLFW init: %s", glfwInitSuccess ? "Success" : "Error");
}

void InputManager::update() {
    glfwPollEvents();

    float delta = 0.0f;
    bool reset = false;
    int resetButton = SettingsConfig::settings.controllerResetZoomButton;

    for (int i = GLFW_JOYSTICK_1; i <= GLFW_JOYSTICK_LAST; ++i) {
        if (glfwJoystickIsGamepad(i)) {
            GLFWgamepadstate gamepadState{};
            glfwGetGamepadState(i, &gamepadState);

            if (gamepadState.buttons[GLFW_GAMEPAD_BUTTON_DPAD_UP] == GLFW_PRESS) {
                delta += (SettingsConfig::settings.reverseDPadZoom ? -1.0f : 1.0f) * SettingsConfig::settings.dPadZoomFactor;
            }
            if (gamepadState.buttons[GLFW_GAMEPAD_BUTTON_DPAD_DOWN] == GLFW_PRESS) {
                delta += (SettingsConfig::settings.reverseDPadZoom ? 1.0f : -1.0f) * SettingsConfig::settings.dPadZoomFactor;
            }
            if (SettingsConfig::settings.enableFastDPad && gamepadState.buttons[GLFW_GAMEPAD_BUTTON_DPAD_RIGHT] == GLFW_PRESS) {
                delta += (SettingsConfig::settings.reverseDPadFastZoom ? -1.0f : 1.0f) * SettingsConfig::settings.dPadZoomFactor * SettingsConfig::settings.fastDPadFactor;
            }
            if (SettingsConfig::settings.enableFastDPad && gamepadState.buttons[GLFW_GAMEPAD_BUTTON_DPAD_LEFT] == GLFW_PRESS) {
                delta += (SettingsConfig::settings.reverseDPadFastZoom ? 1.0f : -1.0f) * SettingsConfig::settings.dPadZoomFactor * SettingsConfig::settings.fastDPadFactor;
            }

            if (resetButton != 0) {
                if (gamepadState.buttons[GLFW_GAMEPAD_BUTTON_A + resetButton - 1]) {
                    reset = true;
                    break;
                }
            }
        }
    }

    if (reset) {
        Logic::resetZoomLevel();
    } else {
        Logic::changeZoom(delta);
    }
}

const HHOOK InputManager::getMouseHook() {
    return mouseHook;
}

void InputManager::cleanUp() {
    glfwTerminate();
}
