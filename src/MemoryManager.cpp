#include <fstream>
#include <libloaderapi.h>
#include <winbase.h>
#include "MemoryManager.h"

float* MemoryManager::baseTPFOV = nullptr;
float* MemoryManager::baseFPFOV = nullptr;
float* MemoryManager::activeBaseFOV = nullptr;
float* MemoryManager::baseModelFOV = nullptr;
float* MemoryManager::currentFOVDelta = nullptr;
float* MemoryManager::targetFOVDelta = nullptr;
float* MemoryManager::FOVAdjustSpeed = nullptr;
float* MemoryManager::currentFPTPFOV = nullptr;
float* MemoryManager::currentModelFOV = nullptr;
char* MemoryManager::isProperFirstPerson = nullptr;
char* MemoryManager::isFirstPerson = nullptr;

__int64 MemoryManager::getBaseAddress() {
    return (unsigned __int64) GetModuleHandleA(NULL);
}

bool MemoryManager::loadMemoryAddresses(std::string &filePath) {
    const char* file_name = filePath.c_str();
//    const char* file_name = "Data/F4SE/Plugins/FineZoomMem.ini";

    auto fileStream = std::ifstream(file_name, std::ifstream::in);
    if (!fileStream.is_open()) {
        return false;
    }

    unsigned __int64 baseAddress = getBaseAddress();

    int baseTPFOVOffset = GetPrivateProfileIntA("GENERAL", "iBaseTPFOV", -1, file_name);
//    if (baseTPFOVOffset == -1) return false; //Optional
    baseTPFOV = reinterpret_cast<float *>(baseAddress + baseTPFOVOffset);

    int baseFPFOVOffset = GetPrivateProfileIntA("GENERAL", "iBaseFPFOV", -1, file_name);
//    if (baseFPFOVOffset == -1) return false; //Optional
    baseFPFOV = reinterpret_cast<float *>(baseAddress + baseFPFOVOffset);

    int activeBaseFOVOffset = GetPrivateProfileIntA("GENERAL", "iActiveBaseFOV", -1, file_name);
    if (activeBaseFOVOffset == -1) return false;
    activeBaseFOV = reinterpret_cast<float *>(baseAddress + activeBaseFOVOffset);

    int baseModelFOVOffset = GetPrivateProfileIntA("GENERAL", "iBaseModelFOV", -1, file_name);
    if (baseModelFOVOffset == -1) return false;
    baseModelFOV = reinterpret_cast<float *>(baseAddress + baseModelFOVOffset);

    int currentFOVDeltaOffset = GetPrivateProfileIntA("GENERAL", "iCurrentFOVDelta", -1, file_name);
    if (currentFOVDeltaOffset == -1) return false;
    currentFOVDelta = reinterpret_cast<float *>(baseAddress + currentFOVDeltaOffset);

    int targetFOVDeltaOffset = GetPrivateProfileIntA("GENERAL", "iTargetFOVDelta", -1, file_name);
    if (targetFOVDeltaOffset == -1) return false;
    targetFOVDelta = reinterpret_cast<float *>(baseAddress + targetFOVDeltaOffset);

    int FOVAdjustSpeedOffset = GetPrivateProfileIntA("GENERAL", "iFOVAdjustSpeed", -1, file_name);
    if (FOVAdjustSpeedOffset == -1) return false;
    FOVAdjustSpeed = reinterpret_cast<float *>(baseAddress + FOVAdjustSpeedOffset);

    int currentFPTPFOVOffset = GetPrivateProfileIntA("GENERAL", "iCurrentFPTPFOV", -1, file_name);
//    if (currentFPTPFOVOffset == -1) return false; //Optional
    currentFPTPFOV = reinterpret_cast<float *>(baseAddress + currentFPTPFOVOffset);

    int currentModelFOVOffset = GetPrivateProfileIntA("GENERAL", "iCurrentModelFOV", -1, file_name);
//    if (currentModelFOVOffset == -1) return false; //Optional
    currentModelFOV = reinterpret_cast<float *>(baseAddress + currentModelFOVOffset);

    int isProperFirstPersonOffset = GetPrivateProfileIntA("GENERAL", "iIsProperFirstPerson", -1, file_name);
//    if (isProperFirstPersonOffset == -1) return false; //Optional
    isProperFirstPerson = reinterpret_cast<char *>(baseAddress + isProperFirstPersonOffset);

    int isFirstPersonOffset = GetPrivateProfileIntA("GENERAL", "iIsFirstPerson", -1, file_name);
    if (isFirstPersonOffset == -1) return false;
    isFirstPerson = reinterpret_cast<char *>(baseAddress + isFirstPersonOffset);

    return true;
}

float &MemoryManager::getBaseTPFOV() {  //TODO remove?
    return *baseTPFOV;
}

float &MemoryManager::getBaseFPFOV() {  //TODO remove?
    return *baseFPFOV;
}

float &MemoryManager::getActiveBaseFOV() {
    return *activeBaseFOV;
}

float &MemoryManager::getBaseModelFOV() {
    return *baseModelFOV;
}

float &MemoryManager::getCurrentFOVDelta() {
    return *currentFOVDelta;
}

float &MemoryManager::getTargetFOVDelta() {
    return *targetFOVDelta;
}

float &MemoryManager::getFOVAdjustSpeed() {
    return *FOVAdjustSpeed;
}

float &MemoryManager::getCurrentFPTPFOV() { //TODO remove?
    return *currentFPTPFOV;
}

float &MemoryManager::getCurrentModelFOV() { //TODO remove?
    return *currentModelFOV;
}

char &MemoryManager::getIsProperFirstPerson() { //TODO remove?
    return *isProperFirstPerson;
}

char &MemoryManager::getIsFirstPerson() {
    return *isFirstPerson;
}
