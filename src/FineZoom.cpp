#include "f4se/PluginAPI.h"
#include "f4se/GameAPI.h"
#include <shlobj.h>
#include <fstream>
#include "../common/IDebugLog.h"

#include "f4se/PluginManager.h"

#include "f4se_common/f4se_version.h"

#include "f4se/PapyrusVM.h"
#include "f4se/PapyrusNativeFunctions.h"

#include "Config.h"

#include "Logic.h"

IDebugLog gLog;

BOOL WINAPI DllMain(HINSTANCE hInst, DWORD reason, LPVOID) {
    if (reason == DLL_PROCESS_ATTACH) {
        gLog.OpenRelative(CSIDL_MYDOCUMENTS, "\\My Games\\Fallout4\\F4SE\\FineZoom.log");
        _MESSAGE("Dll attach begin");
        Logic::setHInstance(hInst);

        _MESSAGE("Dll attach end");
    }

    if (reason == DLL_PROCESS_DETACH) {
        _MESSAGE("Dll deattach begin");

        Logic::cleanUp();

        _MESSAGE("Dll deattach end");
        return 1;
    }

    return 1;
}

extern "C"
{

__declspec(dllexport) bool F4SEPlugin_Query(const F4SEInterface *f4se, PluginInfo *info) {

    _MESSAGE("FineZoom v%s", PLUGIN_VERSION_STRING);
    _MESSAGE("FineZoom query");

    Logic::setInfo(info);

    const char *fileName = "Data/F4SE/Plugins/_FineZoom.dll";

    auto fileStream = std::ifstream(fileName, std::ifstream::in);
    if (fileStream.is_open()) {
        _MESSAGE("_FineZoom.dll detected, skipping runtime version check");
    } else {

        // Check game version
        if (f4se->runtimeVersion < MINIMUM_RUNTIME_VERSION) {
            char str[1024];
            sprintf_s(str, sizeof(str), "Your game version: v%d.%d.%d.%d\nExpected version of at least: v%d.%d.%d.%d\nAnd no greater than: v%d.%d.%d.%d\n%s will be disabled.",
                      GET_EXE_VERSION_MAJOR(f4se->runtimeVersion),
                      GET_EXE_VERSION_MINOR(f4se->runtimeVersion),
                      GET_EXE_VERSION_BUILD(f4se->runtimeVersion),
                      GET_EXE_VERSION_SUB(f4se->runtimeVersion),
                      GET_EXE_VERSION_MAJOR(MINIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_MINOR(MINIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_BUILD(MINIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_SUB(MINIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_MAJOR(MAXIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_MINOR(MAXIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_BUILD(MAXIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_SUB(MAXIMUM_RUNTIME_VERSION),
                      PLUGIN_NAME_LONG
            );

            MessageBox(NULL, str, PLUGIN_NAME_LONG, MB_OK | MB_ICONEXCLAMATION);
            return false;
        }

        if (f4se->runtimeVersion > MAXIMUM_RUNTIME_VERSION) {
            char str[1024];
            sprintf_s(str, sizeof(str), "Your game version: v%d.%d.%d.%d\nExpected version of at least: v%d.%d.%d.%d\nAnd no greater than: v%d.%d.%d.%d\n%s may still work, so will not be automatically disabled.",
                      GET_EXE_VERSION_MAJOR(f4se->runtimeVersion),
                      GET_EXE_VERSION_MINOR(f4se->runtimeVersion),
                      GET_EXE_VERSION_BUILD(f4se->runtimeVersion),
                      GET_EXE_VERSION_SUB(f4se->runtimeVersion),
                      GET_EXE_VERSION_MAJOR(MINIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_MINOR(MINIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_BUILD(MINIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_SUB(MINIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_MAJOR(MAXIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_MINOR(MAXIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_BUILD(MAXIMUM_RUNTIME_VERSION),
                      GET_EXE_VERSION_SUB(MAXIMUM_RUNTIME_VERSION),
                      PLUGIN_NAME_LONG
            );

            MessageBox(NULL, str, PLUGIN_NAME_LONG, MB_OK | MB_ICONEXCLAMATION);
        }
    }

    Logic::query(f4se);

    return true;
}

__declspec(dllexport) bool F4SEPlugin_Load(const F4SEInterface *f4se) {
    _MESSAGE("FineZoom load");

    return Logic::load(f4se);
}
}
