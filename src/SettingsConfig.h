#pragma once

#include <sys/types.h>
#include <sys/stat.h>
#include <ostream>
#ifndef WIN32
#include <unistd.h>
#endif

#ifdef WIN32
#define stat _stat
#endif

class SettingsConfig {
public:
    struct Settings {
        float zoomRate = 1.2f;
        float minZoomFactor = 0.5f;
        bool hasMaxZoomFactor = true;
        float maxZoomFactor = 10.0f;
        int resetZoomOnLowerScopeMode = 0;
        int zoomCurveType = 0; // 0 = Linear, 1 = Sine-like, 2 = Logistic
        bool smoothStep = true;
        bool smoothReset = true;
        bool staticModelGlowIron = true;
        bool staticModelReflex = true;
        bool staticModeSTS = true;
        bool rememberAfterSwitch = true;

        bool reverseScrollDirection = false;
        bool resetZoomWhileNotScope = false;

        float dPadZoomFactor = 0.035f;
        bool enableFastDPad = true;
        float fastDPadFactor = 2.0f;
        bool reverseDPadZoom = false;
        bool reverseDPadFastZoom = false;
        int controllerResetZoomButton = 0;

        int immersionMode = 0;
        bool useIronSights = false;
        bool useImprovedIronSights = false;
        bool useGlowSights = false;
        bool useReflexSights = false;
        bool useProperNormalSights = false;
        bool useProperNVSights = false;
        bool useProperReconSights = false;
        bool useOtherSights = false;
        bool useSTS_Normal = false;
        bool useSTS_NV = false;
        bool useSTS_Recon = false;
        friend std::ostream &operator<<(std::ostream &os, const Settings &settings);
    };
    static Settings settings;

    static void loadSettings();
    static void updateSettings();

private:
    static struct stat fileStat;
    static bool validFileStat;

    static float getFloat(LPCSTR section, LPCSTR name, float defaultValue, LPCSTR fileName);
};

