#include "../common/ITypes.h"
#include "PapyrusFineZoom.h"
#include "Config.h"
#include "../common/IDebugLog.h"

#include "f4se/PapyrusVM.h"
#include "f4se/PapyrusNativeFunctions.h"
#include "Logic.h"

#define F4SE_BASE_PLUGIN_NAME "FineZoom"

namespace PapyrusFineZoom {
    bool IsInstalled(StaticFunctionTag *base) {
        (void) base;
        return true;
    }

    UInt32 GetVersionCode(StaticFunctionTag *base) {
        (void) base;
        return PLUGIN_VERSION;
    }

    void InformWeaponChange(StaticFunctionTag *base) {
        (void) base;
        Logic::switchWeapons();

        // 41 for weapon?
//        for (auto i = 0; i < (*g_player.GetConst())->equipData->kMaxSlots; i++) {
//            _MESSAGE("In slot %d: %p, %p",i, (*g_player.GetConst())->equipData->slots[i].instanceData, (*g_player.GetConst())->equipData->slots[i].item);
//        }

//        (*g_player.GetConst())->equipData->slots[41]

//        _MESSAGE("Via middle data: %p, %p", (*g_player.GetConst())->middleProcess->unk08->equipData->instanceData, (*g_player.GetConst())->middleProcess->unk08->equipData->item);

        _MESSAGE("Weapon Change");
    }

    void InformSightedStateEnter(StaticFunctionTag *base) {
        (void) base;
        Logic::enterSighted();
        _MESSAGE("Sighted state enter");
    }

    void InformSightedStateExit(StaticFunctionTag *base) {
        (void) base;
        Logic::exitSighted();
        _MESSAGE("Sighted state exit");
    }

    void InformGameLoad(StaticFunctionTag *base) {
        (void) base;
        Logic::gameLoad();
    }

    void ResetZoomLevel(StaticFunctionTag *base) {
        (void) base;
        Logic::resetZoomLevel();
    }

    void ZoomIn(StaticFunctionTag *base) {
        (void) base;
        Logic::changeZoom(+1);
    }

    void ZoomOut(StaticFunctionTag *base) {
        (void) base;
        Logic::changeZoom(-1);
    }

    void SetScopeInfo(StaticFunctionTag *base, bool hasIronSights, bool hasImprovedIronSights, bool hasGlowSights, bool hasReflexSights, bool hasScope, bool hasNV, bool hasRecon, bool hasLaserMuzzle) {
        (void) base;
        Logic::setScopeInfo(hasIronSights, hasImprovedIronSights, hasGlowSights, hasReflexSights, hasScope, hasNV, hasRecon, hasLaserMuzzle);
    }

    void InformIsMenuMode(StaticFunctionTag* base, bool value) {
        (void) base;
        Logic::setIsMenuMode(value);
    }

    void InformIsScopeMenu(StaticFunctionTag* base, bool value) {
        (void) base;
        Logic::setIsScopeMenu(value);
    }
}

void PapyrusFineZoom::RegisterFuncs(VirtualMachine *vm) {
    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, bool>("IsInstalled", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::IsInstalled, vm));

    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, UInt32>("GetVersionCode", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::GetVersionCode, vm));

    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, void>("InformWeaponChange", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::InformWeaponChange, vm));

    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, void>("InformSightedStateEnter", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::InformSightedStateEnter, vm));

    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, void>("InformSightedStateExit", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::InformSightedStateExit, vm));

    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, void>("InformGameLoad", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::InformGameLoad, vm));

    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, void>("ResetZoomLevel", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::ResetZoomLevel, vm));

    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, void>("ZoomIn", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::ZoomIn, vm));

    vm->RegisterFunction(
            new NativeFunction0<StaticFunctionTag, void>("ZoomOut", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::ZoomOut, vm));

    vm->RegisterFunction(
            new NativeFunction8<StaticFunctionTag, void, bool, bool, bool, bool, bool, bool, bool, bool>("SetScopeInfo", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::SetScopeInfo, vm));

    vm->RegisterFunction(
            new NativeFunction1<StaticFunctionTag, void, bool>("InformIsMenuMode", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::InformIsMenuMode, vm));

    vm->RegisterFunction(
            new NativeFunction1<StaticFunctionTag, void, bool>("InformIsScopeMenu", F4SE_BASE_PLUGIN_NAME, PapyrusFineZoom::InformIsScopeMenu, vm));

    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "IsInstalled", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "GetVersionCode", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "InformWeaponChange", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "InformSightedStateEnter", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "InformSightedStateExit", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "InformGameLoad", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "ResetZoomLevel", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "ZoomIn", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "ZoomOut", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "SetScopeInfo", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "InformIsMenuMode", IFunction::kFunctionFlag_NoWait);
    vm->SetFunctionFlags(F4SE_BASE_PLUGIN_NAME, "InformIsScopeMenu", IFunction::kFunctionFlag_NoWait);
}