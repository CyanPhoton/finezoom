#pragma once

class MemoryManager {
private:
    static float* baseTPFOV;
    static float* baseFPFOV;

    static float* activeBaseFOV;
    static float* baseModelFOV;
    static float* currentFOVDelta;
    static float* targetFOVDelta;
    static float* FOVAdjustSpeed;

    static float* currentFPTPFOV;
    static float* currentModelFOV;

    static char* isProperFirstPerson;
    static char* isFirstPerson;
public:
    static __int64 getBaseAddress();
    static bool loadMemoryAddresses(std::string &filePath);

    static float& getBaseTPFOV();
    static float& getBaseFPFOV();
    static float& getActiveBaseFOV();
    static float& getBaseModelFOV();
    static float& getCurrentFOVDelta();
    static float& getTargetFOVDelta();
    static float& getFOVAdjustSpeed();
    static float& getCurrentFPTPFOV();
    static float& getCurrentModelFOV();
    static char& getIsProperFirstPerson();
    static char& getIsFirstPerson();
};

