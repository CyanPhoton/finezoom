#pragma once

struct StaticFunctionTag;

class VirtualMachine;

namespace PapyrusFineZoom {
    void RegisterFuncs(VirtualMachine *vm);
}
