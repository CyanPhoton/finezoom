Scriptname FineZoom Const Native Hidden

; Checks to see whether the FineZoom is installed.
bool Function IsInstalled() native global

; Returns the version code of the FineZoom. This value is incremented for every public release of FineZoom.
int Function GetVersionCode() native global

; Tells the plugin that the equipped weapon has changed
Function InformWeaponChange() native global

; Tells the plugin that the sightedStateEnter animation event was triggered
Function InformSightedStateEnter() native global

; Tells the plugin that the sightedStateExit animation event was triggered
Function InformSightedStateExit() native global

; Tells the plugin that the game has loaded as save
Function InformGameLoad() native global

; Tells the plugin to reset the zoom level back to normal
Function ResetZoomLevel() native global

; Tells the plugin to increase the zoom level by 1 step
Function ZoomIn() native global

; Tells the plugin to decrease the zoom level by 1 step
Function ZoomOut() native global

; Tells the plugin of the keywords on the scope
Function SetScopeInfo(bool hasIronSights, bool hasImprovedIronSights, bool hasGlowSights, bool hasReflexSights, bool hasScope, bool hasNV, bool hasRecon, bool hasLaserMuzzle) native global

; Tells the plugin that the game has entered a is menu mode with the give state
Function InformIsMenuMode(bool value) native global

; Tells the plugin if currently in a scope menu
Function InformIsScopeMenu(bool value) native global