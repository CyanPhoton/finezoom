Scriptname ScopedWeaponCheckScript extends ReferenceAlias

Keyword Property dn_HasScope_Ironsights Auto const
Keyword Property dn_HasScope_IronsightsImproved Auto const
Keyword Property dn_HasScope_glowSights Auto const
Keyword Property dn_HasScope_ReflexSight Auto const
Keyword Property dn_HasScope Auto const
Keyword Property dn_HasScope_NightVision Auto const
Keyword Property dn_HasScope_Recon Auto const

Keyword Property dn_HasNightVision Auto const

Keyword Property ma_LaserMusket_Muzzle Auto Const

;Form Property ZM_Standard_Scope_x10 Auto Const

Function UpdateData()

	FineZoom.SetScopeInfo(Game.GetPlayer().WornHasKeyword(dn_HasScope_Ironsights),Game.GetPlayer().WornHasKeyword(dn_HasScope_IronsightsImproved),Game.GetPlayer().WornHasKeyword(dn_HasScope_glowSights),Game.GetPlayer().WornHasKeyword(dn_HasScope_ReflexSight),Game.GetPlayer().WornHasKeyword(dn_HasScope),Game.GetPlayer().WornHasKeyword(dn_HasScope_NightVision) || Game.GetPlayer().WornHasKeyword(dn_HasNightVision),Game.GetPlayer().WornHasKeyword(dn_HasScope_Recon), Game.GetPlayer().WornHasKeyword(ma_LaserMusket_Muzzle))

	; FineZoom.setScopeInfo(Game.GetPlayer().WornHasKeyword(dn_HasScope), Game.GetPlayer().WornHasKeyword(dn_HasScope_ReflexSight), Game.GetPlayer().WornHasKeyword(dn_HasScope_Ironsights) || Game.GetPlayer().WornHasKeyword(dn_HasScope_IronsightImproved) || Game.GetPlayer().WornHasKeyword(dn_HasScope_glowSights))
	
	;if Game.GetPlayer().WornHasKeyword(dn_HasScope)
	;	Debug.Notification("Scopped Weapon")
	;endIf
	
	;if Game.GetPlayer().WornHasKeyword(dn_HasScope_ReflexSight)
	;	Debug.Notification("Has Reflex")
	;endIf
	
	;if Game.GetPlayer().WornHasKeyword(dn_HasScope_Ironsights) || Game.GetPlayer().WornHasKeyword(dn_HasScope_IronsightsImproved) || Game.GetPlayer().WornHasKeyword(dn_HasScope_glowSights)
	;	Debug.Notification("Has Iron Sight")
	;endIf
	
	;if Game.GetPlayer().WornHasKeyword(HasScope)
	;	;Debug.Notification("HasScope")
	;endIf

	;if Game.GetPlayer().WornHasKeyword(dn_HasScope)
		;Debug.Notification("dn_HasScope")
	;endIf

	;if Game.GetPlayer().WornHasKeyword(dn_HasScope_ReflexSight)
		;Debug.Notification("Has Reflex")
	;endIf

	;if Game.GetPlayer().WornHasKeyword(dn_HasScope_Ironsights)
		;Debug.Notification("Has Ironsight")
	;endIf

	;if Game.GetPlayer().WornHasKeyword(dn_HasScope_IronsightsImproved)
		;Debug.Notification("Has Ironsight Improved")
	;endIf

	;if Game.GetPlayer().WornHasKeyword(dn_HasScope_glowSights)
		;Debug.Notification("Has Glowsight")
	;endIf

endFunction

Event OnItemEquipped(Form akBaseObject, ObjectReference akReference)

	;Debug.Notification("w change, worldFOV: " + Game.GetGameSettingFloat("fDefaultWorldFOV:Interface"))

	FineZoom.InformWeaponChange()

	UpdateData()

endEvent

Event OnItemUnequipped(Form akBaseObject, ObjectReference akReference)

	UpdateData()

endEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
  if akSource == Game.GetPlayer()
	if asEventName == "sightedStateEnter"
		;FineZoom.SetAimState(1)
		FineZoom.InformSightedStateEnter()
		;Debug.Notification("sightedStateEnter")
	elseIf asEventName == "sightedStateExit"
		;FineZoom.SetAimState(2)
		FineZoom.InformSightedStateExit()
		;Debug.Notification("sightedStateExit")
	elseIf asEventName == "ReloadComplete"
		;FineZoom.reloadComplete()
		;Debug.Notification("ReloadComplete")		
		;Debug.Notification("FineZoom installed: " + FineZoom.IsInstalled())
	else
   		;Debug.Notification("Unexpected Anim event: " + asEventName)
	endIf

  endIf
endEvent

;Event OnReloadCheck(bool ignore)
;
;	int reloading = GetAnimationVariableInt("isReloading")
;	
;	Debug.Notification("Reloading: " + reloading)
;
;	FineZoom.ReloadCheckResponse(reloading)
;
;EndEvent

Event OnMenuOpenCloseEvent(string asMenuName, bool abOpening)
	;Debug.Notification("IsInMenuMode: " + Utility.IsInMenuMode())
	FineZoom.InformIsMenuMode(Utility.IsInMenuMode())
	If asMenuName == "ScopeMenu"
		FineZoom.InformIsScopeMenu(abOpening)
	EndIf
endEvent

Event OnRaceSwitchComplete()
	UnRegisterForAnimationEvent(Game.GetPlayer(), "sightedStateEnter")
	UnRegisterForAnimationEvent(Game.GetPlayer(), "sightedStateExit")		
	RegisterForAnimationEvent(Game.GetPlayer(), "sightedStateEnter")
	RegisterForAnimationEvent(Game.GetPlayer(), "sightedStateExit")
endEvent

Event OnAnimationEventUnregistered(ObjectReference akSource, string asEventName)
	if (akSource == Game.GetPlayer()) && (asEventName == "sightedStateEnter" || asEventName == "sightedStateExit")
		UnRegisterForAnimationEvent(Game.GetPlayer(), "sightedStateEnter")
		UnRegisterForAnimationEvent(Game.GetPlayer(), "sightedStateExit")		
		RegisterForAnimationEvent(Game.GetPlayer(), "sightedStateEnter")
		RegisterForAnimationEvent(Game.GetPlayer(), "sightedStateExit")
	endIf
endEvent

bool workshopPlusInstalled = False

Event OnPlayerLoadGame()

	workshopPlusInstalled = Game.IsPluginInstalled("WorkshopPlus.esp")

	;Game.GetPlayer().AddItem(Caps001,1000)

	;FineZoom.IsInstalled()

	UpdateData()
	
	;Debug.Notification("Registering for on reload check")

	;RegisterForOnReloadCheck()

	FineZoom.InformGameLoad()

	UnRegisterForAnimationEvent(Game.GetPlayer(), "sightedStateEnter")
	UnRegisterForAnimationEvent(Game.GetPlayer(), "sightedStateExit")
	RegisterForAnimationEvent(Game.GetPlayer(), "sightedStateEnter")
	RegisterForAnimationEvent(Game.GetPlayer(), "sightedStateExit")
	;RegisterForAnimationEvent(Game.GetPlayer(), "reloadStart")
	;RegisterForAnimationEvent(Game.GetPlayer(), "ReloadComplete")

	RegisterForMenues()

EndEvent

Event OnInit() ; This event will run once, when the script is initialized

	workshopPlusInstalled = Game.IsPluginInstalled("WorkshopPlus.esp")
	
	;Game.GetPlayer().AddItem(Caps001,100)

	;FineZoom.IsInstalled()

	UpdateData()
	
	;Debug.Notification("Registering for on reload check")

	;RegisterForOnReloadCheck()

	UnRegisterForAnimationEvent(Game.GetPlayer(), "sightedStateEnter")
	UnRegisterForAnimationEvent(Game.GetPlayer(), "sightedStateExit")
	RegisterForAnimationEvent(Game.GetPlayer(), "sightedStateEnter")
	RegisterForAnimationEvent(Game.GetPlayer(), "sightedStateExit")
	;RegisterForAnimationEvent(Game.GetPlayer(), "reloadStart")
	;RegisterForAnimationEvent(Game.GetPlayer(), "ReloadComplete")

	RegisterForMenues()

EndEvent

Function RegisterForMenues()
	UnRegisterForAllMenuOpenCloseEvents()
  RegisterForMenuOpenCloseEvent("BarterMenu")
  RegisterForMenuOpenCloseEvent("BookMenu")
  RegisterForMenuOpenCloseEvent("Console")
  RegisterForMenuOpenCloseEvent("ConsoleNativeUIMenu")
  RegisterForMenuOpenCloseEvent("ContainerMenu")
  RegisterForMenuOpenCloseEvent("CookingMenu")
  RegisterForMenuOpenCloseEvent("CreditsMenu")
  RegisterForMenuOpenCloseEvent("CursorMenu")
  RegisterForMenuOpenCloseEvent("DialogueMenu")
  RegisterForMenuOpenCloseEvent("ExamineMenu")
  RegisterForMenuOpenCloseEvent("FaderMenu")
  RegisterForMenuOpenCloseEvent("FavoritesMenu")
  RegisterForMenuOpenCloseEvent("GenericMenu")
  RegisterForMenuOpenCloseEvent("HUDMenu")
  RegisterForMenuOpenCloseEvent("LevelUpMenu")
  RegisterForMenuOpenCloseEvent("LoadingMenu")
  RegisterForMenuOpenCloseEvent("LockpickingMenu")
  RegisterForMenuOpenCloseEvent("LooksMenu")
  RegisterForMenuOpenCloseEvent("MainMenu")
  RegisterForMenuOpenCloseEvent("MessageBoxMenu")
  RegisterForMenuOpenCloseEvent("MultiActivateMenu")
  RegisterForMenuOpenCloseEvent("PauseMenu")
  RegisterForMenuOpenCloseEvent("PipboyMenu")
  RegisterForMenuOpenCloseEvent("PromptMenu")
  RegisterForMenuOpenCloseEvent("ScopeMenu")
  RegisterForMenuOpenCloseEvent("SitWaitMenu")
  RegisterForMenuOpenCloseEvent("SleepWaitMenu")
  RegisterForMenuOpenCloseEvent("SPECIALMenu")
  RegisterForMenuOpenCloseEvent("TerminalHolotapeMenu")
  RegisterForMenuOpenCloseEvent("TerminalMenu")
  RegisterForMenuOpenCloseEvent("VATSMenu")
  RegisterForMenuOpenCloseEvent("VignetteMenu")
  RegisterForMenuOpenCloseEvent("WorkshopMenu")
  RegisterForMenuOpenCloseEvent("Workshop_CaravanMenu")
EndFunction
