{
  "modName": "FineZoom",
  "displayName": "Fine Zoom",
  "minMcmVersion": 1,
  "content": [
    {
      "text": "General Settings",
      "type": "section"
    },
    {
      "id": "fZoomRate:General",
      "text": "Zoom Rate - WARNING: Value 1 results in nothing happening",
      "type": "slider",
      "help": "A factor effecting the rate of zoom, applied to scroll wheel, buttons and the d-pad. Range: 1-5, Default: 1.2",
      "valueOptions": {
        "min": 1,
        "max": 5,
        "step": 0.05,
        "sourceType": "ModSettingFloat"
      }
    },
    {
      "id": "fMinZoomFactor:General",
      "text": "Minimum Zoom Factor",
      "type": "slider",
      "help": "The minimum factor that can be applied to the default zoom level. Range: 0-1, Default: 0.5",
      "valueOptions": {
        "min": 0,
        "max": 1,
        "step": 0.05,
        "sourceType": "ModSettingFloat"
      }
    },
    {
      "id": "bHasMaxFactor:General",
      "text": "Enable Maximum Zoom Factor",
      "type": "switcher",
      "help": "Enables having a maximum to the zoom factor which is applied to the default zoom. Default: On",
      "valueOptions": {
        "sourceType": "ModSettingBool"
      },
      "groupControl": 1
    },
    {
      "id": "fMaxZoomFactor:General",
      "text": "Maximum Zoom Factor",
      "type": "slider",
      "help": "The maximum factor that can be applied to the default zoom level. Range: 1-100, Default: 2",
      "valueOptions": {
        "min": 0,
        "max": 100,
        "step": 0.25,
        "sourceType": "ModSettingFloat"
      },
      "groupCondition": 1
    },
    {
      "id": "iResetZoomOnLowerScope:General",
      "text": "Reset Zoom On Lowering Scope Mode",
      "type": "stepper",
      "help": "Enables resting the zoom level when you either completely or partially lower the scope. Default: None",
      "valueOptions": {
        "options": ["None", "Complete", "Partial"],
        "sourceType": "ModSettingInt"
      }
    },
    {
      "id": "bRememberAfterSwitch:General",
      "text": "Enable Remembering Current Zoom Level After Switching Weapons",
      "type": "switcher",
      "help": "(Default: On) Does nothing with Reset Zoom On Lower enabled | NOTE: Doesn't persistent on loading a save (may change in the future).",
      "valueOptions": {
        "sourceType": "ModSettingBool"
      }
    },
    {
      "id": "bResetZoomWhileNotScope:General",
      "text": "Enable Resetting Zoom Level While Not Scoping",
      "type": "switcher",
      "help": "Enables resetting the zoom level with the zoom reset button(s), while not scoping. Default: Off",
      "valueOptions": {
        "sourceType": "ModSettingBool"
      }
    },
    {
      "id": "iZoomCurveType:General",
      "text": "Zoom Curve Type",
      "type": "stepper",
      "help": "Changes the curve of how the zoom level changes as you scope in and out, NOT zoom in an out. Default: Linear",
      "valueOptions": {
        "options": ["Linear", "Sinusoidal", "Logistic"],
        "sourceType": "ModSettingInt"
      }
    },
    {
      "id": "bSmoothStep:General",
      "text": "Enable Smooth Stepping Zoom Level",
      "type": "switcher",
      "help": "Enables smoothly zooming between different zoom levels, doesn't really do much for controllers. Default: On",
      "valueOptions": {
        "sourceType": "ModSettingBool"
      },
      "groupControl": 3
    },
    {
      "id": "bSmoothReset:General",
      "text": "Enable Smooth Zoom Reset",
      "type": "switcher",
      "help": "Enables smoothly zooming back to the default zoom level when the button(s) is/are pressed, requires smooth step. Default: On",
      "valueOptions": {
        "sourceType": "ModSettingBool"
      },
      "groupCondition": 3
    },
    {
      "id": "bStaticModelIronGlow:General",
      "text": "Enable Static Gun Model On Iron and Glow Sights",
      "type": "switcher",
      "help": "Enables making the gun model remain a static size when you zoom in and out, when using iron sights or glow sights. Default: On",
      "valueOptions": {
        "sourceType": "ModSettingBool"
      }
    },
    {
      "id": "bStaticModelReflex:General",
      "text": "Enable Static Gun Model On Reflex Sights",
      "type": "switcher",
      "help": "Enables making the gun model remain a static size when you zoom in and out, when using reflex sights. Default: On",
      "valueOptions": {
        "sourceType": "ModSettingBool"
      }
    },
    {
      "id": "bStaticModelSTS:General",
      "text": "Enable Static Gun Model On See Through Scopes Sights",
      "type": "switcher",
      "help": "Enables making the gun model remain a static size when you zoom in and out, when using see though scopes sights (if installed). Default: On",
      "valueOptions": {
        "sourceType": "ModSettingBool"
      }
    }
  ],
  "pages": [
    {
      "pageDisplayName": "Keyboard Mouse Controls",
      "content": [
        {
          "id": "bReversedScroll:Controls - Mouse",
          "text": "Reverse Scroll Direction",
          "type": "switcher",
          "help": "Switches around the direction you zoom when you scroll. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "FZ_mouseZoomReset",
          "text": "Reset Zoom Hotkey",
          "type": "hotkey",
          "help": "This hotkey will reset the zoom level back to default",
          "valueOptions": {
            "allowModifierKeys": true
          }
        },
        {
          "id": "FZ_mouseZoomIn",
          "text": "Increase Zoom Hotkey",
          "type": "hotkey",
          "help": "This hotkey will increase the zoom level one step, the same as scroll wheel",
          "valueOptions": {
            "allowModifierKeys": true
          }
        },
        {
          "id": "FZ_mouseZoomOut",
          "text": "Decrease Zoom Hotkey",
          "type": "hotkey",
          "help": "This hotkey will decrease the zoom level one step, the same as scroll wheel",
          "valueOptions": {
            "allowModifierKeys": true
          }
        }
      ]
    },
    {
      "pageDisplayName": "Controller Controls",
      "content": [
        {
          "id": "fDPadZoomFactor:Controls - Controller",
          "text": "D-Pad Zoom Speed Factor",
          "type": "slider",
          "help": "A factor scaling the speed of zooming with a D-Pad, on top of Zoom Rate. Range: 0-2, Default: 0.035",
          "valueOptions": {
            "min": 0,
            "max": 2,
            "step": 0.005,
            "sourceType": "ModSettingFloat"
          }
        },
        {
          "id": "bEnableFastDPad:Controls - Controller",
          "text": "Enables Second D-Pad Zoom",
          "type": "switcher",
          "help": "Enables using left and right on the D-Pad to zoom in and out with speed modifier. Default: On",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          },
          "groupControl": 2
        },
        {
          "id": "fFastDPadFactor:Controls - Controller",
          "text": "D-Pad Second Speed Modifier",
          "type": "slider",
          "help": "A factor scaling the speed of zooming with a left/right on a D-Pad, relative to up/down speed. Range: 0-20, Default: 2",
          "valueOptions": {
            "min": 0,
            "max": 20,
            "step": 0.25,
            "sourceType": "ModSettingFloat"
          },
          "groupCondition": 2
        },
        {
          "id": "bReverseDPadZoom:Controls - Controller",
          "text": "Reverse D-Pad Zoom",
          "type": "switcher",
          "help": "Reverses the direction you zoom with up/down on the D-Pad. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bReverseDPadFastZoom:Controls - Controller",
          "text": "Reverse Second D-Pad Zoom",
          "type": "switcher",
          "help": "Reverses the direction you zoom with left/right on the D-Pad. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "iControllerZoomResetButton:Controls - Controller",
          "text": "Controller Zoom Reset Button",
          "type": "stepper",
          "help": "A button on the controller which will reset the zoom once pressed. Default: None",
          "valueOptions": {
            "options": ["None", "A", "B", "X", "Y", "Left Button", "Right Button", "Back", "Start", "Guide", "Left Stick", "Right Stick", "DPad Up", "DPad Right", "DPad Down", "DPad Left"],
            "sourceType": "ModSettingInt"
          }
        }
      ]
    },
    {
      "pageDisplayName": "Immersion",
      "content": [
        {
          "text": "This immersion mode, specifies for which types of scopes the zooming function should be available\nWARNING: After switching to your desired mode or custom options, please switch to another weapon and aim or re-equip your current gun, else the engine gets confused\nThis is experimental, and as such may not work perfectly, it depends on if the particular scope was created properly or not.\nIf you find a scope that it doesn't behave correctly for, do let me know\nReal sights, means sights like long/short and the NV/recon variants",
          "type": "text"
        },
        {
          "id": "iImmersionMode:Immersion",
          "text": "Immersion Mode",
          "type": "stepper",
          "help": "Select the mode of immersion. Default: All Scopes",
          "valueOptions": {
            "options": ["All Scopes", "Real scopes", "Real scopes + See Through", "Custom"],
            "sourceType": "ModSettingInt"
          }
        },
        {
          "text": "Custom Immersion Setting, only active if above is set to \"Custom\"",
          "type": "text"
        },
        {
          "id": "bIronSights:Immersion",
          "text": "Zoom With Iron Sights",
          "type": "switcher",
          "help": "Enables zooming with normal iron sights. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bImprovedIronSights:Immersion",
          "text": "Zoom With Improved Iron Sights",
          "type": "switcher",
          "help": "Enables zooming with improved iron sights. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bGlowSights:Immersion",
          "text": "Zoom With Glow Sights",
          "type": "switcher",
          "help": "Enables zooming with glow sights. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bReflexSights:Immersion",
          "text": "Zoom With Reflex Sights",
          "type": "switcher",
          "help": "Enables zooming with reflex sights. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bProperNormal:Immersion",
          "text": "Zoom With Proper Normal Sights",
          "type": "switcher",
          "help": "Enables zooming with proper normal sights, e.g. Long scope. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bProperNV:Immersion",
          "text": "Zoom With Proper Night Vision Sights",
          "type": "switcher",
          "help": "Enables zooming with proper night vision sights, e.g. short nv scope. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bProperRecon:Immersion",
          "text": "Zoom With Proper Recon Sights",
          "type": "switcher",
          "help": "Enables zooming with proper recon sights, e.g. recon scope. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bOther:Immersion",
          "text": "Zoom With Other Sights",
          "type": "switcher",
          "help": "Enables zooming with other types sights, e.g. Targeting Box on rocket launcher. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bSTS_Normal:Immersion",
          "text": "Zoom With Normal See Through Scopes Sights",
          "type": "switcher",
          "help": "Enables zooming with normal See Through Scopes, e.g. Combat (x8) scope. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bSTS_NV:Immersion",
          "text": "Zoom With Night Vision See Through Scopes Sights",
          "type": "switcher",
          "help": "Enables zooming with See Through Scopes night vision sights, (if they exist). Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        },
        {
          "id": "bSTS_Recon:Immersion",
          "text": "Zoom With Recon See Through Scopes Sights",
          "type": "switcher",
          "help": "Enables zooming with See Through Scopes recon sights, e.g. Combat (x2.5) recon scope. Default: Off",
          "valueOptions": {
            "sourceType": "ModSettingBool"
          }
        }
      ]
    }
  ]
}